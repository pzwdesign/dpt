<?php

namespace App\Services;

use Carbon\Carbon;

class DeactivationService
{
    const REMAINING_DAYS_IN_PERCENT = 15;

    public function checkRemainingDays($daysInPeriod, $remainingDays): bool
    {
        $percentageRemainingDays = $daysInPeriod > 0 ? $remainingDays / $daysInPeriod : 0;

        return $percentageRemainingDays < self::REMAINING_DAYS_IN_PERCENT / 100;
    }

    public function checkUnderpacing($order, $delivered): bool
    {
        $periodAppLimit = $order->period_app_limit;
        $timeSincePeriodStart = Carbon::parse($order->period_from)->diffInSeconds(Carbon::now());
        $timePeriodFull = Carbon::parse($order->period_from)->diffInSeconds($order->period_to);

        $x = $delivered / $periodAppLimit;
        $y = $timeSincePeriodStart / $timePeriodFull;

        return $x < $y;
    }
}
