<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DeliveryService
{
    protected DeactivationService $deactivationService;

    public function __construct()
    {
        $this->deactivationService = new DeactivationService();
    }

    /**
     * Update daily app limit
     */
    public function updateOrderDailyAppLimit()
    {
        $clients = $this->getClients();

        foreach ($clients as $client) {
            $orders = $this->getOrdersByClient($client);

            foreach ($orders as $order) {
                $this->setDailyAppLimit($order, $this->getDailyAppLimit($client, $order));
            }
        }
    }

    protected function getClients(): array
    {
        return DB::select('select * from `client`');
    }

    protected function getDailyAppLimit($client, $order): int|null
    {
        $daysInPeriod = Carbon::parse($order->period_from)->diffInDays($order->period_to);
        $remainingDays = Carbon::parse($order->period_to)->diffInDays(Carbon::now());

        $periodAppLimit = $order->period_app_limit;

        $deliveries = $this->getDeliveriesByOrder($client, $order);
        $delivered = count($deliveries);

        // If there's just 15% (or less) of the period left, we don't want to apply pacing.
        if ($this->deactivationService->checkRemainingDays($daysInPeriod, $remainingDays)) {
            return null;
        }

        // If the client is not likely to hit their period limit, we don't apply pacing.
        if ($this->deactivationService->checkUnderpacing($order, $delivered)) {
            return null;
        }

        $remainingDeliveryCount = $periodAppLimit - $delivered;

        if($remainingDeliveryCount < 0) {
            $remainingDeliveryCount = 0;
        }

        return (int)($remainingDeliveryCount / $remainingDays);
    }

    protected function getOrdersByClient(object $client): array
    {
        return DB::select("select * from `order` where `client_id` = {$client->id}");
    }

    /**
     * Get deliveries by order
     *
     * @param object $client
     * @param object $order
     *
     * @return array
     */
    protected function getDeliveriesByOrder(object $client, object $order): array
    {
        $periodFrom = $order->period_from;
        $periodTo = $order->period_to;

        return DB::select("
                    select * from `delivery`
                    where `client_id` = {$client->id}
                      and `delivery_date` >= '{$periodFrom}'
                      and `delivery_date` <= '{$periodTo}'");
    }

    protected function setDailyAppLimit(object $order, int|null $value)
    {
        if($value < 0) {
            $value = 0;
        }

        $value = $value || $value === 0 ? $value : 'NULL';

        DB::update("update `order` set daily_app_limit = {$value} where id = {$order->id}");
    }
}
